class Journey {
  constructor(settings){
    this.id = this.guid();
    this.floorJourneyCreatedOn = 0;
    this.currentElevatorFloor = 0; //0 unkown numbered for actual floor
    this.direction = 0; // 0 is unkown 1 is up, -1 is down
    this.destinationFloor = null;//optional

    this.state = 0; // 0 represents waiting
    this.init(settings);
  }
  init(settings){
    Object.assign(this, settings);
  }

  goingUp(){
    return this.direction === 1;
  }

  goingDown(){
    return this.direction === -1;
  }

  states(state = null){
    const states = {
        0: 'WAITING',
        1: 'COMPLETE',
      };

      if (state){
        return states[state];
      }else{
        return states;
      }
  }
  getAllState(){
    return this.states();
  }

  getCurrentState(){
    return this.states(this.state);
  }

  setCurrentState(state){
    this.state = state;
  }

  resolve(){
    this.state = 1;
  }

  guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
  }
}


export default Journey;
