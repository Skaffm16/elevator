import Journey from './Journey';
class Elevator {

    constructor() {
        this.floors = {
            current: 0,
            total: 5,
            destination: 0,
            toTravel: 0,
        }
        this.time = {
            startTime: 0,
            arrivalWaitTime: 2000,
            departureWaitTime: 4000,
            arrivalDepartureWaitTime: 6000,
        }
        this.speed = 2000;
        this.doorStatus = 0;
        //this.running = 0;
        this.state = 0;
        this.states = {
          idle : 0,
          running : 1,
          unloading :2,
          loading: 3,
        };
        this.queue = [];

        this.DOM = {
            floorButtons: {
                one: document.querySelector('[data-button="1"]'),
                two: document.querySelector('[data-button="1"]'),
                three: document.querySelector('[data-button="1"]'),
                four: document.querySelector('[data-button="1"]'),
                five: document.querySelector('[data-button="1"]'),
            }
        }

        this.stateIs = this.getState; //get alias for getState

        // this.state = 1;
        // this.floors.current = 1;
        // this.handleDirectionButtonPressed(-1, 3);
        // this.handleDirectionButtonPressed(1, 4);
        // this.handleDirectionButtonPressed(1, 2);
        // this.destroyJourney(this.queue[1].id);
        // console.log(this.queue);

    }
    init(){
      this.bindFloorDirectionButtons();
      this.bindFloorDestinationButtons();
    }

    //machine representation of our state from string
    getState(state = 'idle'){
      let value;

      let keys = Object.keys(this.states);

      keys.forEach((k) => {
        if(state == k){
          value = this.states[k];
        }
      });

      return value;
    }
    // get the english representation of our numeric state
    getReadableState(state = 0){
      let key;

      for (var property in this.states){
        if (this.states.hasOwnProperty(property)){
          key = property;
        }
      }
      return key;
    }

    //start traveling to specified floor
    travel() {

    }

    //diff between current and destination floors
    calculateFloorToTravel() {

    }

    //open doors
    openDoors() {
      console.log('doors open');
    }

    // close doors
    closeDoors() {

    }

    // is current floor equal to the destination floor
    hasReachedDestination() {

    }

    handleDestinationButtonPressed(floor){
      // figures out what floor is pressed and where it needs to go.
      // creates a second journey queue,
      // comb thru and kill the ones that already went to that floor
      let differenceBetweenFloors = this.floors.current - this.floors.destination;
      let journey = this.createNewJourney(this.floors.current, differenceBetweenFloors, this.floors.current);

      console.log(floor);

      this.createNewJourney()

    }

    // first press on the elevator to determine direction
    handleDirectionButtonPressed(direction = 1, floor = 1){
      let journey = this.createNewJourney(floor, direction, this.floors.current);
      //we are on current floor
      if(this.floors.current == floor){
        //only open doors when idle
        if(this.state == this.stateIs('idle') ){
          //open doors
          this.openDoors();
        } else if ( this.state == this.stateIs('loading') || this.state == this.stateIs('unloading')  ){
          //destroy journey by 'id'
          this.destroyJourney(journey);
        } else if(this.state == this.stateIs('running')){
          this.queue.push(journey);
        }
      }else{
        this.queue.push(journey);
      }

      console.log(this.queue);

    }

    createNewJourney(floorJourneyCreatedOn, direction, currentElevatorFloor, destinationFloor = null, state = 0){
      const journey = new Journey({
        floorJourneyCreatedOn,
        direction,
        currentElevatorFloor,
        destinationFloor,
        state,
        });
        return journey;
    }

    destroyJourney(id){
      //remove journey from queue
      this.queue.forEach((journey, n)=>{
        if(journey.id == id){
          this.queue.splice(n ,1)
        }
      });
    }

    bindFloorDestinationButtons(){
      // this is similar to  below but with diff dom elements
      const destinationFloorButtons = document.querySelectorAll('button[data-destination-floor]');
      console.log(destinationFloorButtons)
      Array.prototype.forEach.call(destinationFloorButtons, (element) =>{
        element.addEventListener('click', () =>{
          const floor = element.dataset.destinationFloor;
          this.handleDestinationButtonPressed(floor);
        });
      });
    }

    bindFloorDirectionButtons(){
     const floorButtons =  document.querySelectorAll('button[data-floor]');
      Array.prototype.forEach.call(floorButtons, (element) =>{
        element.addEventListener('click', () =>{
          const direction = element.dataset.direction;
          const floor = element.dataset.floor;
          this.handleDirectionButtonPressed(direction, floor);
        });
      });
    }

}


export const elevator = new Elevator();

elevator.init();
